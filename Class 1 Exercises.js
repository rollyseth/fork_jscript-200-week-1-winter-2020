/**
 * MATH
 */

// 1. Pagliacci charges $16.99 for a 13” pizza and $19.99 for a 17” pizza.
// What is the area for each of these pizzas?
// (radius would be the listed size - i.e. 13" - divided by 2)


const areaSmallPizza =Math.PI*((13/2)**2);
const areaLargePizza= Math.PI *((17/2) **2);

console.log("********** TASK 1 ********** \n");
console.log("Area of the 13\" pizza is:" + areaSmallPizza );
console.log("Area of the 17\" pizza is:" + areaLargePizza );

// 2. What is the cost per square inch of each pizza?
const costPerSqInchSmall =(16.99/areaSmallPizza).toFixed(2); 
const costPerSqInchLarge =(19.99/areaLargePizza).toFixed(2);
 

console.log(`Cost per sq. inch of the small (13 inch) pizza is ${costPerSqInchSmall}`);
console.log(`Cost per sq. inch of a large (17 inch) pizza is ${costPerSqInchLarge}`);


// 3. Using the Math object, put together a code snippet
// that allows you to draw a random card with a value
// between 1 and 13 (assume ace is 1, jack is 11…)

console.log("********** TASK 2 ********** \n");

const card1=Math.round((Math.random()*12)+1); //not including number 1 
const card2=Math.round((Math.random()*12)+1);
const card3=Math.round((Math.random()*12)+1);
console.log("Three cards are:"+card1,card2, card3);

// 4. Draw 3 cards and use Math to determine the highest
// card


console.log("Highest card is:"+Math.max(card1,card2,card3));


/**
 * ADDRESS LINE
 */

// 1. Create variables for firstName, lastName,
// streetAddress, city, state, and zipCode. Use
// this information to create a formatted address block
// that could be printed onto an envelope.

console.log("********** TASK 3 ********** \n");
const firstName ='Rolly';
const lastName='Seth';
const streetAddress='40th St.';
const city='Redmond';
const state='Washington';
const zipCode='98052';

console.log ("Envelope Style Address Printing: \n")
console.log(firstName+' '+lastName+', '+streetAddress+', '+city+', '+state+', '+zipCode);

// 2. You are given a string in this format:
// firstName lastName(assume no spaces in either)
// streetAddress
// city, state zip(could be spaces in city and state)
// 
// Write code that is able to extract the first name from this string into a variable.
// Hint: use indexOf, slice, and / or substring


const combinedName =`${firstName} ${lastName}`;
const combinedAddress =`
${combinedName}, 
40th St.
Redmond, WA 99052`;

console.log("Combined Address \n",combinedAddress);

const firstSpaceIndex= combinedAddress.indexOf(' ');
const extractedFirstName=combinedAddress.substring(
    0,
    firstSpaceIndex
);
console.log(`Extracted First Name is: ${extractedFirstName}`);

/**
 * FIND THE MIDDLE DATE
 */
// On your own find the middle date(and time) between the following two dates:
// 1/1/2020 00:00:00 and 4/1/2020 00:00:00
//
// Look online for documentation on Date objects.

// Starting hint:
console.log("********** TASK 4 ********** \n");
const startDate= new Date(2020, 0, 1,0,0,0);
const endDate = new Date(2020, 3, 1,0,0,0);

const midDate= new Date((endDate.getTime()+ startDate.getTime())/2);
console.log(`Middle Date is: ${midDate}`);



